

const express = require('express')

const dotenv = require('dotenv')

const mongoose = require('mongoose')

const app = express()

const port = 3001

//To initialize the dotenv in order to use for our application
dotenv.config()


//MONGOOSE CONNECTION
//Mongoose uses the 'connect' function to connect to the cluster in our MongoDB Atlas
/*
	It takes 2 arguments:
	1. Connection string from MongoDB Atlas
	2. Object that contains the middleware/standard that MongoDB uses
*/
mongoose.connect(`mongodb+srv://ivybacudo:${process.env.MONGODB_PASSWORD}@cluster0.merm9.mongodb.net/?retryWrites=true&w=majority`, 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)
//Initialize the mongoose connection to the MongoDB database by assigning 'mongoose.connection' to the 'db' variable
let db = mongoose.connection
//Listen to the event of the connection by using the '.on()' of the mongoose.connection. Then, log details in the console based on the event (error or success - open)
db.on('error', console.error.bind(console, "Connection Error"))
db.on('open', () => console.log('Connected to MongoDB!'))


//CREATING A SCHEMA

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String, 
		default: 'Pending'
	}
})

//CREATINGG A MODEL
const Task = mongoose.model('Task', taskSchema)

//CREATING THE ROUTES
app.use(express.json())
app.use(express.urlencoded({extended: true}))

//Create a single task - route
app.post('/tasks', (req, res) => {
	// Use the task model to find a similar entry in the database nased on the user input from the req.body (or from postman)
	Task.findOne({name: req.body.name}, (error, result) => {

		/*
		//Handle errors and sucesses by using the 'error' and 'result' variables and check if one of the containes anything
		//If error variable contains an error, return a response with the error
		if(error){
			return res.send(error)
		}
		*/

		// Check if the name from postman body has a similar entry in the database. If there is a similar entry, return response saying that a duplicate task has been found
		if(result !== null && result.name == req.body.name){
			return res.send('Duplicate task found!')
		} else {
			//If there are no duplicate entries, the create a new task out of the request.body and save it to the database.
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save((error, savedTask) => {
				if(error){
					return console.error(error)
				}
				//If there are no errors upon saving, return a response with a status of 201
				return res.status(201).send('New Task Created!')
			})
		}
	})
})

// Get all tasks - route
app.get('/tasks', (req, res) => {
	return Task.find({}, (error, result) => {
		if(error){
			res.send(error)
		}
		res.send(result)
	})
})


// USERS COLLECTION
// Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

// Model
const User = mongoose.model('User', userSchema)


// Routes
app.post('/register', (req, res) => {
	User.findOne({ username: req.body.username}, (error, result) => {
		//Check for duplicates
		if(result != null && result.username == req.body.username) {
			return res.send('Duplicate user found!')
			//If no duplicates
		} else {
			//Checkk if the username and the password contains a string, if it is both empty the code will go to the else statement
			if(req.body.username !== '' && req.body.password !== ''){
				// If both username and password contains value, then continue with the process of saving that user as a new user in the database
				let newUser = new User({
					username: req.body.username,
					password: req.body.password
				})
				newUser.save((error, savedUser) =>{
					if(error){
						return res.send(error)
					}
					return res.send('New User registered successfully!')
				})
			} else {
				return res.send(`BOTH Username and Password must be provided.`)
			}
		}
	})
})


app.get('/users', (req, res) => {
	return User.find({}, (error, result) => {
		if(error){
			res.send(error)
		}
		res.send(result)
	})
})

//ROUTE FOR DELETE A SINGLE USER

app.delete('/delete-user')
app.put('/delete-user-password', (req, res) => {
	User.findOne({username: req.body.username}, () => {
		User.updateOne({password})
	})
})

//app.listen should ALWAYS be at the bottom of the server
app.listen(port, () => console.log(`Server is running at port ${port}`))




